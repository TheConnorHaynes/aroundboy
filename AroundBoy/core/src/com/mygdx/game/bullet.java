package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

/**
 * Created by Dylan on 9/23/2017.
 */

public class bullet {
    float x;
    float y;
    float width;
    boolean lose;
    Sound loss;

    public bullet(float x, float y){
        this.x = x;
        this.y = y;
        width = 26;
        lose = false;
        loss = Gdx.audio.newSound(Gdx.files.internal("Hitsound.wav"));
    }

    public void move(float arg){
        y += -arg;
        if(y < 0){
            if(lose == false)
                loss.play();
            lose = true;
            y = 0;
        }
    }

    public void curve(shield cannon, float arg){
        y += -arg;
        x = cannon.x;
        if(y < 0){
            if(lose == false)
                loss.play();
            lose = true;
            y = 0;
        }
    }
}
