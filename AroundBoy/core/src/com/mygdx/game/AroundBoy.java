package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.util.Random;


public class AroundBoy extends ApplicationAdapter {
	SpriteBatch batch;
	Music song1;
	Music song2;
	Sound block;
	Sound hit;
	Texture img;
	Texture bullet;
	Texture player;
	Texture shield;
	Texture wall;
	Texture frame;
	TextureRegion[][] splitFrame;

	float time = 0;
	int multiplier;

	shield platform1;
	shield platform2;
	shield cannon1;
	shield cannon2;
	bullet pewPew;
	bullet pewPew2;

	float checkPoint;
	float runningTime;
	float score;

	ShaderProgram shader;
	BitmapFont font;
	SpriteBatch text;
	FrameBuffer fbo;
	SpriteBatch frameBatch;
	ShaderProgram frameShader;
	
	@Override
	public void create () {
		frameShader = new ShaderProgram(Gdx.files.internal("framevertex.vsh").readString(), Gdx.files.internal("framepixel.fsh").readString());
		frameBatch = new SpriteBatch();
		frameBatch.setShader(frameShader);
		fbo = new FrameBuffer(Pixmap.Format.RGB565, 1280, 720, false);
		splitFrame = new TextureRegion[10][10];

		block = Gdx.audio.newSound(Gdx.files.internal("Blocksound.wav"));
		hit = Gdx.audio.newSound(Gdx.files.internal("Hitsound.wav"));
		song1 = Gdx.audio.newMusic(Gdx.files.internal("Accel.wav"));
		song2 = Gdx.audio.newMusic(Gdx.files.internal("Velocity.wav"));
		song1.setLooping(true);
		song1.play();
		song2.setLooping(true);
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		text = new SpriteBatch();
		shader = new ShaderProgram(Gdx.files.internal("vertex.vsh").readString(), Gdx.files.internal("pixel.fsh").readString());

		batch = new SpriteBatch();

		img = new Texture(Gdx.files.internal("badlogic.jpg"));
		bullet = new Texture(Gdx.files.internal("bullet.png"));
		player = new Texture(Gdx.files.internal("player.png"));
		shield = new Texture(Gdx.files.internal("shield.png"));
		wall = new Texture(Gdx.files.internal("wall.png"));

		platform1 = new shield(0);
		platform2 = new shield(320.5f);
		cannon1 = new shield(0);
		cannon2 = new shield(100);
		pewPew = new bullet(cannon1.x, 300);
		pewPew2 = new bullet(cannon2.x, 500);
		batch.setShader(shader);
		System.out.println(frameShader.getLog());
		multiplier = 1;


	}

	@Override
	public void render () {
		fbo.begin();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		time = Gdx.graphics.getDeltaTime();
		runningTime+=time;
		if(!pewPew.lose && !pewPew2.lose) {
			score += time * multiplier;
		}

		if(song1.getPosition() > 5 && song1.getPosition() < 5.03f){
			multiplier += 1.5;
		}
		if(song1.getPosition() > 14 && song1.getPosition() < 14.03f){
			multiplier += 2;
		}
		if(song1.getPosition() > 36 && song1.getPosition() < 62){
			pewPew.move(0);
			if(pewPew.y < 800){
				pewPew.move(-(2+multiplier));
			}
			pewPew2.curve(cannon2,5.2f);
		}
		else{
			pewPew.move(2+multiplier);
			if(pewPew2.y < 800){
				pewPew2.curve(cannon2,-5.2f);
			}
		}

		batch.begin();
		shader.setUniformf("u_time", runningTime);
		shader.setUniformf("u_score", score);
		System.out.println(runningTime);
		batch.draw(shield, platform1.x, 70);
		batch.draw(shield, platform1.x+32, 70);
		batch.draw(shield, platform1.x-32, 70);
		batch.draw(shield,platform2.x, 70);
		batch.draw(shield,platform2.x+32, 70);
		batch.draw(shield,platform2.x-32, 70);
		batch.draw(bullet,pewPew.x,pewPew.y);
		batch.draw(bullet,cannon2.x,pewPew2.y);

		for(float i = 0; i < 650; i=i+32){
			batch.draw(player, i, 0);
		}
		batch.end();

		fbo.end();
		frame = fbo.getColorBufferTexture();
		frameBatch.begin();
		frameShader.setUniformf("u_time", runningTime);
		frameShader.setUniformf("u_score", score);
		//frameBatch.draw(frame,0,0);
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				splitFrame[i][j] = new TextureRegion(frame, j*128,i*72, 128, 72);
				frameBatch.draw(splitFrame[i][j], j*128, 720-i*72-72);
			}
		}
		frameBatch.end();
		text.begin();
		font.draw(text, "score: "+(int)(score*10), 50,50);


		if(Gdx.input.isKeyPressed(Input.Keys.D)){
			platform1.move(-10);
			platform2.move(-10);
		}
		if(Gdx.input.isKeyPressed(Input.Keys.A)){
			platform1.move(10);
			platform2.move(10);
		}
		cannon1.turretTeleport();
		cannon2.move(4);

		//pewPew2.curve(cannon2,1);

		if(platform1.collide(pewPew.x-32,pewPew.y+20,96,32)||platform2.collide(pewPew.x-32,pewPew.y+20,96,32)){
			pewPew.y = 500;
			pewPew.x = cannon1.x;
			block.play();
		}
		if (platform1.collide(pewPew2.x-32,pewPew2.y+20,96,32)||platform2.collide(pewPew2.x-32,pewPew2.y+20,96,32)){
			pewPew2.y = 500;
			pewPew2.x = cannon2.x;
			block.play();
		}
		if(pewPew.lose || pewPew2.lose){
			font.draw(text, "Press R to Restart", 1280-150, 50);
			runningTime = 0;
			song1.stop();
			pewPew.y = 0;
			pewPew2.y = 0;
			if(Gdx.input.isKeyPressed(Input.Keys.R)){
				score = 0;
				time = 0;
				multiplier = 1;
				runningTime =0;
				pewPew.lose = false;
				pewPew2.lose = false;
				pewPew.y = 500;
				pewPew2.y = 500;
				platform1.x = 0;
				platform2.x = 320f;
				song1.play();
			}
		}
		text.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		bullet.dispose();
		player.dispose();
		shield.dispose();
		wall.dispose();
	}
}
