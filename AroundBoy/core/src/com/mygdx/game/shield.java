package com.mygdx.game;

import java.util.Random;

/**
 * Created by Dylan on 9/23/2017.
 */

public class shield {
    float x;
    float width;

    public shield(float x){
        this.x = x;
        width = 96;
    }

    public void move(float arg){
        x += arg;
        if(x < 0){
            x = x + 640;
        }
        if(x > 640){
            x = x - 640;
        }
    }

    public void turretTeleport(){
        float randomFloat = 0 + new Random().nextFloat() * 650;
        x = randomFloat;
    }

    public boolean collide(float x, float y, float width, float height){
        return this.x < x+width && this.x+32 > x && 70 < y+height && 102 > y;
    }
}
