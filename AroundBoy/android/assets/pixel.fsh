#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;
varying vec4 opos;
uniform sampler2D u_texture;
uniform float u_time;
uniform float u_score;
void main()
{
  vec4 color = v_color * texture2D(u_texture, v_texCoords);
  color.r = (1.0+sin(u_time*5.0+opos.x*6.28/640.0)*(1.0-v_texCoords.y))/2.0;
  color.g = (1.0+cos(u_time*5.0+opos.x*6.28/640.0)*(1.0-v_texCoords.y))/2.0;
  //color.g = sin(u_time);
  color.b = 1.0-(1.0+sin(u_time*5.0+opos.x*6.28/640.0)*(1.0-v_texCoords.y))/2.0;
  gl_FragColor = color;
}
