#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;
uniform mat4 u_projTrans;
uniform float u_time;
uniform float u_score;
varying vec4 v_color;
varying vec2 v_texCoords;
varying vec4 pos;
varying vec4 opos;

void main()
{
   pos = a_position;
   opos = pos;
   v_color = a_color;
   v_color.a = v_color.a * (255.0/254.0);
   v_texCoords = a_texCoord0;

   if(u_score > 10.0 && u_time>0.0){
        pos.x += 15.0*sin(distance(pos.x+u_time, 1280.0/2.0));
   }

   gl_Position =  u_projTrans * pos;
}


