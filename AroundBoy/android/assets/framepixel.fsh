#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;
varying vec4 opos;
uniform sampler2D u_texture;
uniform float u_time;
uniform float u_score;
void main()
{
  vec4 color = v_color * texture2D(u_texture, v_texCoords);

  if(u_time > 14.5){
    for(float i = 0.0; i < 10.0; i+=1.0){
            if((distance( i/10.0, v_texCoords.x)<.001 || distance( i/10.0, v_texCoords.y)<.001) && color.r == 0.0 && color.g == 0.0 && color.b == 0.0){
                color.r = .25*(1.0+sin(u_time*16.0));
                color.b = .25*(1.0+sin(u_time*16.0));
                color.g = .25*(1.0+sin(u_time*16.0));
          }
        }
       }



  gl_FragColor = color;
}